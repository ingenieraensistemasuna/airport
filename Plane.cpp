#include "Plane.h"
#include <QPixmap>

Plane::Plane()
{
    this->id = 0;
    this->labelPlane = nullptr;
}

Plane::Plane(QLabel *labelPlane, int id, int x, int y)
{
    this->labelPlane = labelPlane;
    this->state = StatePlane::LANDING;
    this->id = id;
    this->x = x;
    this->y = y;
    this->labelPlane->setScaledContents(true);
    this->labelPlane->setGeometry(this->x, this->y, 100, 120);
    this->labelPlane->setVisible(false);
    this->initPixmap = this->labelPlane->pixmap()->copy();
}

Plane::~Plane(){}

QLabel *Plane::getLabelPlane(){
    return this->labelPlane;
}

void Plane::setLabelPlane(QLabel *labelPlane){
    this->labelPlane = labelPlane;
}

int Plane::getId(){
    return this->id;
}

void Plane::setId(int id){
    this->id = id;
}

StatePlane Plane::getSate(){
    return this->state;
}

void Plane::setState(StatePlane state){
    this->state = state;
}


int Plane::getPositionX(){
    return this->x;
}

int Plane::getPositionY(){
    return y;
}

void Plane::setPositionX(int x){
    this->x = x;
}

void Plane::setPositionY(int y){
    this->y = y;
}

void Plane::setPosition(int x, int y){
    this->x = x;
    this->y = y;
}


void Plane::rightRotate(){
    QMatrix rm;
    rm.rotate(90);
    QPixmap pixmap(*this->labelPlane->pixmap());
    pixmap = pixmap.transformed(rm);
    this->labelPlane->setPixmap(pixmap);
}

void Plane::leftRotate(){
    QMatrix rm;
    rm.rotate(-90);
    QPixmap pixmap(*this->labelPlane->pixmap());
    pixmap = pixmap.transformed(rm);
    this->labelPlane->setPixmap(pixmap);
}

void Plane::reset(){
    this->state = StatePlane::LANDING;
    this->labelPlane->setPixmap(this->initPixmap);
    this->labelPlane->move(140, 250);
    this->x = 140;
    this->y = 250;
    this->labelPlane->setVisible(false);
}




