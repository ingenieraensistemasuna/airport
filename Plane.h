#ifndef PLANE_H
#define PLANE_H

#include <QString>
#include <QLabel>
#include "StatePlane.h"

class Plane
{
private:

    StatePlane state;
    QLabel *labelPlane;
    QPixmap initPixmap;
    int x;
    int y;
    int id;

public:
    Plane();

    Plane(QLabel *labelPlane, int id, int x, int y);

    ~Plane();

    QLabel *getLabelPlane();

    void setLabelPlane(QLabel *labelPlane);

    int getId();

    void setId(int id);

    StatePlane getSate();

    void setState(StatePlane state);

    int getPositionX();

    int getPositionY();

    void setPositionX(int x);

    void setPositionY(int y);

    void setPosition(int x, int y);

    void rightRotate();

    void leftRotate();

    void reset();


};

#endif // PLANE_H
