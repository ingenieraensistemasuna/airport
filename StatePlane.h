#ifndef STATEPLANE_H
#define STATEPLANE_H

enum class StatePlane
{
    LANDING,
    WAITING,
    PARKING,
};

#endif // STATEPLANE_H
