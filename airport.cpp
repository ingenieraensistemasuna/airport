#include "airport.h"
#include "mainWindow.h"

Airport::Airport(){
    this->arriveOne = nullptr;
    this->arriveTwo = nullptr;
    this->parkingOne = new Stack();
    this->parkingTwo = new Stack();
    this->arrivePlanes = new Queue();
    this->waitToLeave = new Queue();
    bool isOpen = true;
    this->isOpen = &isOpen;
}

Airport::Airport(Queue *queueInitPlanes, bool *isOpen, TrafficLight *trafficLight){
    this->arriveOne = nullptr;
    this->arriveTwo = nullptr;
    this->parkingOne = new Stack();
    this->parkingTwo = new Stack();
    this->waitToLeave = new Queue();
    this->arrivePlanes = queueInitPlanes;
    this->isOpen = isOpen;
    this->trafficLight = trafficLight;
}


Queue *Airport::getArrivetPlanes(){
    return this->arrivePlanes;
}

Stack *Airport::getParkingOne(){
    return this->parkingOne;
}

Node *Airport::getArriveOne(){
    return this->arriveOne;
}

void Airport::setArriveOne(Node *node){
    this->arriveOne = node;
}

Node *Airport::getArriveTwo(){
    return this->arriveTwo;
}

void Airport::setArriveTwo(Node *node){
    this->arriveTwo = node;
}

bool *Airport::getIsOpen(){
    return this->isOpen;
}

void Airport::setIsOpen(bool *isOpen){
    this->isOpen = isOpen;
}


// Función padre que hace el ciclo del aeropuerto mientras este abierto
void Airport::openAirport(){
    *this->isOpen = true;
    while(*this->isOpen){

        // Aterrizar los aviones
        while(Node *aux = this->arrivePlanes->pop()){
            if(*this->isOpen){
                this->move(aux);
            } else {
                break;
            }
        }

        // Mover a cola de espera para despegar el parqueo 2
        Main::delay(200);
        while(Node *aux = this->parkingTwo->pop()){
            if(*this->isOpen){
                this->move(aux);
            } else {
                break;
            }
        }

        // Despachar la cola de espera
        Main::delay(200);
        while(Node *aux = this->waitToLeave->pop()){
            if(*this->isOpen){
                this->move(aux);
            } else {
                break;
            }
        }

        // Mover a cola de espera para despegar el parqueo 1
        Main::delay(200);
        while(Node *aux = this->parkingOne->pop()){
            if(*this->isOpen){
                this->move(aux);
            } else {
                break;
            }
        }

        // Despachar la cola de espera
        Main::delay(200);
        while(Node *aux = this->waitToLeave->pop()){
            if(*this->isOpen){
                this->move(aux);
            } else {
                break;
            }
        }

    }
}

// Despacha los aviones segun el estado en que esten
void Airport::move(Node* aux){
    Plane *plane = aux->getPlane();
    switch (plane->getSate()) {
    case StatePlane::LANDING : {

        int street = 1 + rand() % 2;
        Plane *plane = aux->getPlane();
        int initPositionY;

        if(street == 1){
            initPositionY = 200;
            this->arriveOne = aux;
            plane->setPositionY(initPositionY);

        } else if(street == 2){
            initPositionY = 150;
            this->arriveTwo = aux;
            plane->setPositionY(initPositionY);
        }
        Engine *engine = new Engine(aux, this->isOpen);
        this->arriveMove(engine);

    }
        break;
    case StatePlane::PARKING :{

        Engine *engine = new Engine(aux, this->isOpen);
        this->waitingMode(engine);

    }
        break;
    case StatePlane::WAITING :{

        Engine *engine = new Engine(aux, this->isOpen);
        this->leaveMode(engine);

    }
        break;
    }
}

// Se encarga de mover el avion hasta el semaforo y esperar si el semaforo esta en rojo
void Airport::arriveMove(Engine *engine){
    if(this->isOpen){
        Node *node = engine->getNode();
        engine->moveHorizontalTo(320);
        // ESPERAR SEMAFORO DE PISTA 1
        while( node == this->arriveOne && !this->trafficLight->getLightOneState()){
            Main::delay(10);
        }
        // ESPERAR SEMAFORO DE PISTA 2
        while( node == this->arriveTwo && !this->trafficLight->getLightTwoState()){
            Main::delay(10);
        }
        this->parkingMove(engine);
    }
}

// Se encarga de mover el Avion al parqueo 1 si no esta lleno y si lo esta, lo inserta al 2
void Airport::parkingMove(Engine *engine){
    if(*this->isOpen){
        Plane *plane = engine->getNode()->getPlane();
        engine->moveHorizontalTo(430, 50);
        plane->rightRotate();//
        if(this->parkingOne->getLength() < 5){
            engine->moveVerticalTo(440);
            plane->leftRotate();

            int limitMoveHorizotal = 830;

            if(engine->getNode()->getPrevious() != nullptr){
                Plane *previousPlane = engine->getNode()->getPrevious()->getPlane();
                limitMoveHorizotal = previousPlane->getPositionX() - 60;
            }
            engine->moveHorizontalTo(limitMoveHorizotal);
            plane->setState(StatePlane::PARKING);
            this->arriveOne = nullptr;
            this->parkingOne->push(engine->getNode());

        } else {
            engine->moveVerticalTo(390);
            plane->leftRotate();

            int limitMoveHorizotal = 830;
            if(this->parkingTwo->getLength() >= 1){
                Plane *previousPlane = engine->getNode()->getPrevious()->getPlane();
                limitMoveHorizotal = previousPlane->getPositionX() - 60;
            }
            engine->moveHorizontalTo(limitMoveHorizotal);
            plane->setState(StatePlane::PARKING);
            this->arriveTwo = nullptr;
            this->parkingTwo->push(engine->getNode());
        }
    }
}

// Se encarga de mover el avion a la posición de despegar
void Airport::waitingMode(Engine *engine){
    if(*this->isOpen){
        this->arriveOne = nullptr;
        this->arriveTwo = nullptr;
        Plane *plane = engine->getNode()->getPlane();
        engine->moveHorizontalTo(510);
        plane->leftRotate();

        int limitMoveVertical = 140;
        if(this->waitToLeave->getLength() >= 1){
            Plane *previousPlane = engine->getNode()->getPrevious()->getPlane();
            limitMoveVertical = previousPlane->getPositionY() + 60;
        }
        engine->moveVerticalTo(limitMoveVertical);
        plane->setState(StatePlane::WAITING);
        this->waitToLeave->push(engine->getNode());
    }
}

// Se encarga de despegar los aviones de la pista
void Airport::leaveMode(Engine *engine){
    if(*this->isOpen){
        Plane *plane = engine->getNode()->getPlane();
        engine->moveVerticalTo(-20, 30);
        plane->reset();
        if(this->arrivePlanes->getLength() == 0){
            engine->getNode()->setPrevious(nullptr);
        }
        if(this->arrivePlanes->getLength() == 10){
            engine->getNode()->setNext(nullptr);
        }
        this->arrivePlanes->push(engine->getNode());
        Main::delay(500);
    }
}






















