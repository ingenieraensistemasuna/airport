#ifndef MOVE_H
#define MOVE_H

#include <QTime>
#include "queue.h"
#include "stack.h"
#include "node.h"
#include "trafficlight.h"
#include "engine.h"

class Airport : QObject
{
    Q_OBJECT

private:

    Queue *arrivePlanes;
    Queue *waitToLeave;
    Node *arriveOne;
    Node *arriveTwo;
    Stack *parkingOne;
    Stack *parkingTwo;
    bool *isOpen;
    TrafficLight *trafficLight;

public:

    Airport();
    Airport(Queue *arrivePlanes, bool *isOpen, TrafficLight *trafficLight);

    void openAirport();

    void move(Node* node);

    void arriveMove(Engine *engine);

    void parkingMove(Engine *engine);

    void waitingMode(Engine *engine);

    void leaveMode(Engine *engine);

    Queue *getArrivetPlanes();

    Node *getArriveOne();

    void setArriveOne(Node *node);

    Node *getArriveTwo();

    void setArriveTwo(Node *node);

    Stack *getParkingOne();

    bool *getIsOpen();

    void setIsOpen(bool *isOpen);

    void reset();

};

#endif // MOVE_H
