#include "engine.h"
#include "mainWindow.h"

Engine::Engine(Node *node, bool *isOpen)
{
    this->node = node;
    this->isOpen = isOpen;
}

// Mueve el avion en el eje X
void Engine::moveHorizontalTo(int limitMove, int speed, int interval){
    Plane *plane = this->node->getPlane();
    QLabel *label_plane = plane->getLabelPlane();
    int move = plane->getPositionX();
    bool isReverse = move > limitMove;
    label_plane->setVisible(true);
    while (move != limitMove) {
        if(*this->isOpen){
            label_plane->move(move, plane->getPositionY());
            label_plane->repaint();
            Main::delay(speed);
            if(!isReverse){
                move += interval;
            } else {
                move -= interval;
            }
        } else {
            break;
        }
    }
    plane->setPositionX(move);
}

// Mueve el avion en el eje Y
void Engine::moveVerticalTo(int limitMove, int speed, int interval){
    Plane *plane = this->node->getPlane();
    QLabel *label_plane = plane->getLabelPlane();
    int move = plane->getPositionY();
    bool isReverse = move > limitMove;
    label_plane->setVisible(true);
    while (move != limitMove) {
        if(*this->isOpen){
            label_plane->move(plane->getPositionX(), move);
            label_plane->repaint();
            Main::delay(speed);
            if(!isReverse){
                move += interval;
            } else {
                move -= interval;
            }
        } else {
            break;
        }
    }
    plane->setPositionY(move);
}

void Engine::setNode(Node *node){
    this->node = node;
}

Node *Engine::getNode(){
    return this->node;
}
