#ifndef ENGINE_H
#define ENGINE_H

#include "node.h"
#include "Plane.h"
class Engine
{
private:
    Node *node;
    bool *isOpen;
public:
    Engine(Node *node, bool *isOpen);
    void moveHorizontalTo(int limitMove, int speed = 100, int interval = 10);
    void moveVerticalTo(int limitMove, int speed = 100, int interval = 10);
    void setNode(Node *node);
    Node *getNode();

};

#endif // ENGINE_H
