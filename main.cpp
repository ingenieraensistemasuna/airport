#include "mainWindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    srand(time(nullptr));
    QApplication a(argc, argv);
    Main w;
    w.show();
    return a.exec();
}
