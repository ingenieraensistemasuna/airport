#include "ui_mainWindow.h"
#include "mainWindow.h"

Main::Main(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::Main)
{
    ui->setupUi(this);
    ui->pushButton_3->setEnabled(false);
    this->trafficLight = new TrafficLight(ui->radioButton, ui->radioButton_2, true);
    this->initPlanes();
    resize(1000, 650);

    // Establece un timer para que cambie el estado del semaforo cada 3 segundos
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(onTrafficLightsChange()));
    timer->start(3000);
}

Main::~Main()
{
    delete ui;
}

void Main::initPlanes(){
    QLabel label;
    QPixmap pixmap(":plane.png");

    Queue *initPlanes = new Queue();

    this->ui->label_plane_1->setPixmap(pixmap);

    initPlanes->push( new Node( new Plane( this->ui->label_plane_1, 10, 140, 250 ) ) );

    this->ui->label_plane_2->setPixmap(pixmap);

    initPlanes->push( new Node( new Plane( this->ui->label_plane_2, 11, 140, 250 ) ) );

    this->ui->label_plane_3->setPixmap(pixmap);

    initPlanes->push( new Node( new Plane( this->ui->label_plane_3, 12, 140, 250 ) ) );

    this->ui->label_plane_4->setPixmap(pixmap);

    initPlanes->push( new Node( new Plane( this->ui->label_plane_4, 13, 140, 250 ) ) );

    this->ui->label_plane_5->setPixmap(pixmap);

    initPlanes->push( new Node( new Plane( this->ui->label_plane_5, 14, 140, 250 ) ) );

    this->ui->label_plane_6->setPixmap(pixmap);

    initPlanes->push( new Node( new Plane( this->ui->label_plane_6, 15, 140, 200 ) ) );

    this->ui->label_plane_7->setPixmap(pixmap);

    initPlanes->push( new Node( new Plane( this->ui->label_plane_7, 16, 140, 200 ) ) );

    this->ui->label_plane_8->setPixmap(pixmap);


    initPlanes->push( new Node( new Plane( this->ui->label_plane_8, 17, 140, 200 ) ) );

    this->ui->label_plane_9->setPixmap(pixmap);


    initPlanes->push( new Node( new Plane( this->ui->label_plane_9, 18, 140, 200 ) ) );

    this->ui->label_plane_10->setPixmap(pixmap);

    initPlanes->push( new Node( new Plane( this->ui->label_plane_10, 19, 140, 200 ) ) );

    bool isOpen = true;
    this->open = &isOpen;
    airport = new Airport(initPlanes, this->open, this->trafficLight);

}

// Lisener que se encarga de cambiar el estado del semaforo
void Main::onTrafficLightsChange(){
    this->trafficLight->setState();
}


// Funcion que pausa la ejecución del programa sin congelar la pantalla
void Main::delay(int milis){
    QCoreApplication::processEvents();
    QTime dieTime= QTime::currentTime().addMSecs(milis);
        while (QTime::currentTime() < dieTime)
            QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
}

// Empieza el proceso del aeropuerto
void Main::on_pushButton_2_clicked()
{
    if(this->airport){
        ui->pushButton_2->setEnabled(false);
        ui->pushButton_3->setEnabled(true);
        airport->openAirport();
    }
}

// Finaliza el proceso del aeropuerto
void Main::on_pushButton_3_clicked()
{
    if(this->airport){
        *this->open = false;
        ui->pushButton_3->setEnabled(false);
    }
}
