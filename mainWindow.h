#ifndef MAIN_H
#define MAIN_H

#include <QMainWindow>
#include "airport.h"
#include "queue.h"
#include "node.h"
#include "trafficlight.h"
#include <QTimer>

QT_BEGIN_NAMESPACE
namespace Ui { class Main; }
QT_END_NAMESPACE

class Main : public QMainWindow
{
    Q_OBJECT

    Airport *airport;
    bool *open;
    TrafficLight *trafficLight;

public:
    Main(QWidget *parent = nullptr);
    ~Main();

    void initPlanes();
    void static delay(int secs);

public slots:

    void onTrafficLightsChange();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

private:
    Ui::Main *ui;
};
#endif // MAIN_H
