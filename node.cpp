#include "node.h"

Node::Node()
{
    this->plane = nullptr;
    this->next = nullptr;
    this->previous = nullptr;
}

Node::Node(Plane* plane){
    this->plane = plane;
    this->next = nullptr;
    this->previous = nullptr;
}

Node::~Node(){
    this->plane->~Plane();
}

void Node::SetPlane(Plane* plane){
    this->plane = plane;
}

Plane* Node::getPlane(){
    return this->plane;
}

void Node::setPrevious(Node* previous){
    this->previous = previous;
}
Node* Node::getPrevious(){
    return this->previous;
}

void Node::setNext(Node* next){
    this->next = next;
}

Node* Node::getNext(){
    return this->next;
}
