#ifndef NODE_H
#define NODE_H


#include "Plane.h"

class Node
{
private:

    Plane * plane;
    Node * previous;
    Node * next;

public:

    Node();

    Node(Plane *plane);

    ~Node();

    void SetPlane(Plane* plane);

    Plane* getPlane();

    void setPrevious(Node* previous);

    Node *getPrevious();

    void setNext(Node*next);

    Node *getNext();
};

#endif // NODE_H
