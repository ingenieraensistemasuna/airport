#include "queue.h"

Queue::Queue()
{
    this->head=nullptr;
    this->last = nullptr;
    this->length = 0;
}

bool Queue::isEmpty(){
    return this->head == nullptr;
}

void Queue::push(Node* node){
    if(!this->isEmpty()){
        last->setNext(node);
        node->setPrevious(last);
        last=node;
    } else {
        head=node;
        last=node;
    }
    length++;
}

Node* Queue::pop(){
    if(!this->isEmpty()){
        Node* aux = this->head;
        if (this->last == this->head) {
            this->head = this->last = nullptr;
        } else {
            this->head = this->head->getNext();
        }
        length--;
        return aux;
    }
    return nullptr;
}

Node* Queue::getHead(){
    return this->head;
}

Node* Queue::getLast(){
    return this->last;
}

int Queue::getLength(){
    return this->length;
}

void Queue::reset(){
    Node *aux = this->head;
    if(!this->isEmpty()){
        while (aux) {
            aux->getPlane()->reset();
            aux = aux->getNext();
        }
        this->head = nullptr;
        this->last = nullptr;
    }
}

void Queue::clone(Queue *queue){
    Node *aux = queue->getHead();
    while (aux) {
        this->push(new Node(aux->getPlane()));
        aux = aux->getNext();
    }
}
