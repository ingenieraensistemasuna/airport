#ifndef QUEUE_H
#define QUEUE_H
#include <node.h>


class Queue
{
private:
    Node* head;
    Node* last;
    int length;
public:

    bool isEmpty();

    void push(Node* node);

    Node* pop();

    Node* getHead();

    Node* getLast();

    int getLength();

    void reset();

    void clone(Queue *queue);

    Queue();

};

#endif // QUEUE_H
