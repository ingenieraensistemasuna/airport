#include "stack.h"

Stack::Stack()
{
    this->head = nullptr;
    this->last = nullptr;
    this->length = 0;
}


bool Stack::isEmpty(){
    return this->head == nullptr;
}

void Stack::push(Node* node){
    if(!this->isEmpty()){
        node->setNext(this->head);
        this->head->setPrevious(node);
        this->head = node;
    } else {
        this->head = this->last = node;
    }
    this->length++;
}

Node* Stack::pop(){
    if(!this->isEmpty()){
        Node* aux = this->head;
        if (this->last == this->head) {
            this->head = this->last = nullptr;
        } else {
            this->head = this->head->getNext();
        }
        this->length--;
        return aux;
    }
    return nullptr;
}

Node* Stack::getHead(){
    return this->head;
}

Node* Stack::getLast(){
    return this->last;
}

int Stack::getLength(){
    return this->length;
}
