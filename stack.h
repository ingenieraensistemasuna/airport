#ifndef STACK_H
#define STACK_H

#include "node.h"

class Stack
{
private:

    Node* head;
    Node* last;
    int length;

public:

    Stack();

    bool isEmpty();

    void push(Node* node);

    Node* pop();

    Node* getHead();

    Node* getLast();

    int getLength();

};

#endif // STACK_H
