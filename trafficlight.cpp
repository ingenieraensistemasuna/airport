#include "trafficlight.h"
#include "mainWindow.h"

TrafficLight::TrafficLight(QRadioButton *lightOne, QRadioButton *lightTwo, bool initState)
{
    this->lightOne = lightOne;
    this->lightTwo = lightTwo;
    this->state = &initState;
}

// Hace el intercambio de luces
void TrafficLight::toggle(){
    bool traffic = *this->state;
    if(traffic){
        this->lightTwo->setChecked(false);
        this->lightTwo->update();
        this->lightOne->setChecked(true);
        this->lightOne->click();
        *this->state = false;
    } else {
        this->lightOne->setChecked(false);
        this->lightOne->update();
        this->lightTwo->setChecked(true);
        this->lightTwo->click();
        *this->state = true;
    }
}

// Metodo llamado desde un lisener del mainWidget
void TrafficLight::setState(){
    //*this->state = !*this->state;
    toggle();
}

bool TrafficLight::getState(){
    return *this->state;
}

bool TrafficLight::getLightOneState(){
    return this->lightOne->isChecked();
}

bool TrafficLight::getLightTwoState(){
    return this->lightTwo->isChecked();
}
