#ifndef TRAFFICLIGHT_H
#define TRAFFICLIGHT_H
#include <QRadioButton>

class TrafficLight
{
private:
    QRadioButton *lightOne;
    QRadioButton *lightTwo;
    bool *state;
public:
    TrafficLight(QRadioButton *lightOne, QRadioButton *lightTwo, bool initState);
    void setState();
    bool getState();
    void toggle();
    bool getLightOneState();
    bool getLightTwoState();
};

#endif // TRAFFICLIGHT_H
